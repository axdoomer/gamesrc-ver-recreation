#!/usr/bin/gawk -f

BEGIN {
	# Define here the input filename, and prefix and suffix for outputs
	IN_FILENAME = "ENGINE.C";
	OUT_PREFIX = "ENG_";
	OUT_SUFFIX = ".C";

	# Define here a limited set of vars (e.g., local vars) to choose from
	split("i j k l dx p", input_vars);
#	split("i j k l y1 y2 dx dy p", permuted_vars);

	# Define here the macro names. For each possible ordered subset of
	# input_vars with the size of output_macros, a corresponding line
	# is printed by this script.
	split("CM_I CM_J CM_K CM_L CM_DX CM_P", output_macros);

	# Define here the possible maximum values of additional macros
	# macros, all sharing a common minimum value of 0.  The choice
	# of value for each macro is picked independently of the order,
	# or of any of the preceding macros.
	split("1 1 1 1", extra_macros_max_vals);

	# Extra macros names
#	split("CM_PART1", extra_macros);
	split("CM_PART1 CM_PART2 CM_PART3 CM_PART4", extra_macros);

	if ((length(output_macros)>length(input_vars)) ||
	    (length(extra_macros)!=length(extra_macros_max_vals))) {
		printf("Assertion failed! Please check your script.\n");
		exit;
	}

	# Following is a state machine which simulates N-K loops over N-K
	# loop vars, of lengths M,M-1,M-2...

	for (i=1;i<=length(output_macros);++i) {
		permute_loop_vars[i]=0; # Loop vars before adjustments for dups
		permute_loop_vars_max[i]=length(input_vars)-i+1; # Max. value of each loop var
		offsetted_permute_loop_vars[i]=0; # After adjustments
	}

	# Handling the extra macros is simpler
	extra_macros_num_of_combos=1;
	for (i=1;i<=length(extra_macros_max_vals);++i) {
		extra_macros_num_of_combos*=(extra_macros_max_vals[i]+1);
	}
#	printf("DEBUG extra_macros_num_of_combos %s\n", extra_macros_num_of_combos);

	printf("#!/bin/bash\n");
	i=1;
	while (i>0) {
		if (i>length(permute_loop_vars)) {
			commonprefix = "sed '";
			for (j=1;j<=length(permute_loop_vars);++j) {
				commonprefix = commonprefix sprintf("s/^#define %s .*/#define %s %s/;", output_macros[j], output_macros[j], input_vars[offsetted_permute_loop_vars[j]]);
#				commonprefix = commonprefix sprintf("%s %s;", output_macros[j], input_vars[offsetted_permute_loop_vars[j]]);
			}
			for (j=0;j<extra_macros_num_of_combos;++j) {
				printf("%s",commonprefix);
				tempprod = 1;
				for (k=1;k<=length(extra_macros);++k) {
#					printf("DEBUG j %s tempprod %s extra_macros_max_vals[k] %s;", j, tempprod, extra_macros_max_vals[k]);
					curr_index = int((int(j) / tempprod) % (extra_macros_max_vals[k]+1));
					tempprod *= int(extra_macros_max_vals[k]+1);
					printf("s/^#define %s .*/#define %s %s/;", extra_macros[k], extra_macros[k], curr_index);
#					printf("%s %s;", extra_macros[k], curr_index);
				}
				printf("' %s | todos > %s%d%s\n", IN_FILENAME, OUT_PREFIX, linecount++, OUT_SUFFIX);
			}
			--i;
		} else {
			if (permute_loop_vars[i] == permute_loop_vars_max[i]) {
				permute_loop_vars[i] = 0;
				offsetted_permute_loop_vars[i] = 0;
				--i;
			} else {
				++permute_loop_vars[i];
				# Inefficient way to ensure there's no duplication
				++offsetted_permute_loop_vars[i];
				do {
					for (j=1;j<i;++j) {
						if (offsetted_permute_loop_vars[i] == offsetted_permute_loop_vars[j]) {
							++offsetted_permute_loop_vars[i];
							break;
						}
					}
				} while (j<i);
				++i;
			}
		}
	}		
}
